

#!/bin/bash

# any future command that fails will exit the script
set -e


# Output something like /usr/bin/pm2

whereis pm2 stop 0

# Delete the old repo
rm -rf nodeapps/scrapper

cd nodeapps/

# clone the repo again
sudo git clone https://oauth2:$ACCESS_TOKEN@gitlab.com/snobin/scrapper.git

cd scrapper/

npm run pm2